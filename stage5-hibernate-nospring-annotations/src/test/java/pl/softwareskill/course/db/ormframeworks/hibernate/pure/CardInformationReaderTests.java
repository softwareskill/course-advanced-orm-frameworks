package pl.softwareskill.course.db.ormframeworks.hibernate.pure;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.UUID.randomUUID;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import static pl.softwareskill.course.db.ormframeworks.hibernate.CardCountry.DE;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationLogger;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationReader;

public class CardInformationReaderTests {

    private final CardInformationLogger logger = mock(CardInformationLogger.class);
    private final PureHibernateNoTransactionsCardRepository cardRepository = mock(PureHibernateNoTransactionsCardRepository.class);
    private final CardInformationReader reader = new CardInformationReader(logger, cardRepository);

    @Test
    void shouldLogCardOwnerData() {
        //given : existing card
        String cardId = existingCard();
        //when: request for card information data
        reader.printCardData(cardId);
        //then: card data is displayed
        verify(logger, times(1)).logCardData(any(Card.class));
    }

    private String existingCard() {
        String cardId = randomUUID().toString();
        when(cardRepository.findById(cardId)).thenReturn(of(new Card(cardId, cardId, cardId, true, DE)));
        return cardId;
    }

    @Test
    void shouldLogCardDataNotFound() {
        //given : not existing card
        String cardId = notExistingCard();
        //when: request for card information data
        reader.printCardData(cardId);
        //then: card data not found
        verify(logger, times(1)).logCardDataNotFound(eq(cardId));
    }

    private String notExistingCard() {
        String cardId = randomUUID().toString();
        when(cardRepository.findById(cardId)).thenReturn(empty());
        return cardId;
    }
}
