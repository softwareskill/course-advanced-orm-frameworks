package pl.softwareskill.course.db.ormframeworks.hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Data;

@Embeddable
@Data
class UserEmail {

    @Column(name = "EMAIL_ID")
    String emailId;

    @Column(name = "EMAIL_ADDRESS")
    String emailAddress;

//    @Column(name = "USER_ID")
//    String userId;
}
