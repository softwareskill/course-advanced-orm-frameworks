package pl.softwareskill.course.db.ormframeworks.springdata;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje Hibernate w trybie JPA z poziomu Spring
 * z wykorzystaniem CrudRepository ze SpringData
 *
 * Uruchomienie wymaga podania jednego parametru - inentyfikatora karty do wyszukania
 */
@SpringBootApplication
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class SpringDataApplication implements CommandLineRunner {

    CardInformationReader cardInformationReader;

    public static void main(String[] args) {

        //KOnfiguracja w ramach Spring - zobacz appltcation.properties, klasy anotowane @Configuration, @Repository
        SpringApplication app = new SpringApplicationBuilder()
                .sources(SpringDataApplication.class)
                .web(WebApplicationType.NONE)
                .build();
        app.run(args).close();
    }

    @Override
    public void run(String... args) {
        if (args.length == 1) {
            cardInformationReader.printCardData(args[0]);
        } else {
            log.error("Wymagany jeden parametr ");
        }
    }
}
