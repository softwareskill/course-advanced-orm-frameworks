package pl.softwareskill.course.db.ormframeworks.hibernate;

import org.hibernate.SessionFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CardInformationReaderIntegrationTests {

    private static SessionFactory sessionFactory;

    private final CardInformationLogger logger = Mockito.spy(new CardInformationLogger());
    private CardInformationReader reader;

    @BeforeAll
    static void initDatabase() {
        sessionFactory = DatabaseInitializer.initializeFromHbmXml();
    }

    @BeforeEach
    void init() {
        CardRepository cardRepository = new CardRepository(sessionFactory);
        reader = new CardInformationReader(logger, cardRepository);
    }

    @Test
    void shouldLogCardOwnerData() {
        //given : existing card
        String cardId = existingCard();
        //when: request for card information data
        reader.printCardData(cardId);
        //then: card data is displayed
        verify(logger, times(1)).logCardData(any(Card.class));
    }

    private static String existingCard() {
        return "100";
    }

    @Test
    void shouldLogCardDataNotFound() {
        //given : not existing card
        String cardId = notExistingCard();
        //when: request for card information data
        reader.printCardData(cardId);
        //then: card data not found
        verify(logger, times(1)).logCardDataNotFound(eq(cardId));
    }

    private static String notExistingCard() {
        return "notExisting";
    }
}
