package pl.softwareskill.course.db.ormframeworks.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
class EntityDetachCaller {

    CardRepository cardRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public void detachTest(String cardId) {
        cardRepository.findById(cardId)
                .ifPresent(card -> {

                    entityManager.detach(card);

                    entityManager.remove(card);
                });
    }
}
