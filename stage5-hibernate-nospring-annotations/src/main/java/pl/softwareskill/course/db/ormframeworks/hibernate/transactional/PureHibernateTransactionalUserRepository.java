package pl.softwareskill.course.db.ormframeworks.hibernate.transactional;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.DatabaseOperationException;
import pl.softwareskill.course.db.ormframeworks.hibernate.User;
import pl.softwareskill.course.db.ormframeworks.hibernate.UserRepository;

@RequiredArgsConstructor
class PureHibernateTransactionalUserRepository implements UserRepository {

    private final Session session;

    @Override
    public Optional<User> findById(String userId) {
        try {
            User value = session.get(User.class, userId);
            return Optional.ofNullable(value);
        } catch (Exception e) {
            throw new DatabaseOperationException(e);
        }
    }
}