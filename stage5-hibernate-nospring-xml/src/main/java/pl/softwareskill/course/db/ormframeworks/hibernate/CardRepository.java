package pl.softwareskill.course.db.ormframeworks.hibernate;

import static java.util.Objects.nonNull;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

@RequiredArgsConstructor
class CardRepository {

    private final SessionFactory factory;

    Optional<Card> findById(String cardId) {
        Transaction tx = null;
        try (var session = factory.openSession()) {
            tx = session.getTransaction();
            return Optional.ofNullable(session.get(Card.class, cardId));
        } catch (Exception e) {
            rollback(tx);
            tx = null;
            throw new pl.softwareskill.course.db.ormframeworks.hibernate.DatabaseOperationException(e);
        } finally {
            commit(tx);
        }
    }

    private void commit(Transaction tx) {
        if (nonNull(tx) && tx.isActive()) {
            tx.commit();
        }
    }

    private void rollback(Transaction tx) {
        if (nonNull(tx) && tx.isActive()) {
            tx.rollback();
        }
    }
}