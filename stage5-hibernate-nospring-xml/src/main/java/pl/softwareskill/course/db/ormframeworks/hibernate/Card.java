package pl.softwareskill.course.db.ormframeworks.hibernate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
class Card {
    String cardId;
    String cardUuid;
    String cardOwner;
    Boolean enabled;
    CardCountry cardCountry;
}
