# README #

Aplikacje demo SoftwareSkill dla modułu obsługi baz danych - część dotycząca prostych popularnych frameworków takich jak Hibernate oraz abstrakcji JPA oraz Spring Data JPA.

### Po co to repozytorium? ###

Repozytorium prezentuje sposób konfiguracji i wykorzystania API danego frameworka  
 
* Wersja 1.0

### Opis ###
* Funkcjonalność - aplikacje w ramach modułów realizują tą samą kogikę, wyszukują w bazie danych karty o podanym identyfikatorze i wyświetlają jej dane. W przypadku gdy karta nie zostanie znaleziona wyświetlana jest informacja iż danych dla danego identyfikatora nie znaleziono.
* Baza danych - baza danych zawierająca informacje o kartach i użytkownikach. Zobacz skrypty sql.
* Konfiguracja - nie ma konieczności konfiguracji. Wykorzystywana jest baza H2 w trybie in memory.  
* Zależności -  H2 (baza danych in memory), PostgreSQL, Mockito, JUnit 5, Spring Boot, Logback (framework do logów aplikacyjnych).
* Jak uruchomić testy - polecenie mvn test lub uruchomić z poziomu IDE (np. Run All tests w IntelliJ dla modułu).
* Jak uruchomić aplikację - z linii poleceń klasy, mając w nazwie Application - zobacz opis w komenarzu tych klas. 

### Z kim się kontaktować? ###

* Właściciel repozytorium kodu - SoftwareSkill
* Autorzy rozwiązania - Krzysztof Kądziołka krzysztof.kadziolka@gmail.com