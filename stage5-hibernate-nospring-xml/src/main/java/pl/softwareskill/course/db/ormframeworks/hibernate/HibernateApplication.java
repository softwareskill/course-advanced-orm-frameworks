package pl.softwareskill.course.db.ormframeworks.hibernate;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje Hibernate w trybie nmatywnym z konfiguracją Hibertnate z poziomu kodu
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class HibernateApplication {

    public static void main(String[] args) {
        CardInformationLogger logger = new CardInformationLogger();

        //Inicjalizacja/konfiguracja Hibernate poprzez kod, gdzie część konfiguracji
        //jest wstrzykiwana z poziomu kodu
        var sessionFactory = DatabaseInitializer.initializeFromProperties();

        CardRepository cardRepository = new CardRepository(sessionFactory);
        CardInformationReader cardInformationReader = new CardInformationReader(logger, cardRepository);

        cardInformationReader.printCardData(args[0]);
        sessionFactory.close();
    }

}
