package pl.softwareskill.course.db.ormframeworks.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateInitializer {

    public static SessionFactory initialize() {
        try {
            Configuration config = new Configuration().configure();
            return config.buildSessionFactory();
        } catch (Throwable ex) {
            throw new DatabaseOperationException(ex);
        }
    }

    public static SessionFactory initializeForPostgreSql() {
        try {
            Configuration config = new Configuration().configure("hibernate.postgres.cfg.xml");
            return config.buildSessionFactory();
        } catch (Throwable ex) {
            throw new DatabaseOperationException(ex);
        }
    }
}
