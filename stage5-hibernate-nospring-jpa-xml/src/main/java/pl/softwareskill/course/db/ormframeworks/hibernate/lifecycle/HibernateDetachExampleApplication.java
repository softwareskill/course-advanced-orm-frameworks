package pl.softwareskill.course.db.ormframeworks.hibernate.lifecycle;

import javax.persistence.Persistence;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardCountry;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje Hibernate w trybie JPA dla stanu encji detached.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext
 * oraz co się stanie po wywołaniu detach.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class HibernateDetachExampleApplication {

    public static void main(String[] args) {
        //Konfiguracja - zobacz META-INF/persistence.xml
        var ef = Persistence.createEntityManagerFactory("SoftwareSkill");

        var em = ef.createEntityManager();

        var tx = em.getTransaction();
        tx.begin();

        var card = em.find(Card.class, "1");
        //Zobacz co się stanie dla poniższych przypadków (transient)
        //var card = new Card();
        //card.setCardId("1");//istniejąca ale transient
        //card.setCardId("12222222");//nie istniejąca transient

        em.detach(card);
        //em.clear();//Clear czyści cały PC

        card = em.merge(card);//Po merge użyj tego co zwraca merge
        //card = em.find(Card.class, "1");

        card.setCardCountry(CardCountry.PL);

        em.flush();

        tx.rollback();

        em.close();

        ef.close();
    }
}
