package pl.softwareskill.course.db.ormframeworks.hibernate.lifecycle;

import javax.persistence.Persistence;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje Hibernate w trybie JPA dla stanu encji managed.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext
 * oraz co się stanie po wywołaniu find.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class HibernateGetExampleApplication {

    public static void main(String[] args) {

        //Konfiguracja - zobacz META-INF/persistence.xml
        var ef = Persistence.createEntityManagerFactory("SoftwareSkill");

        var em = ef.createEntityManager();

        var card = em.find(Card.class, "1");

        em.close();

        ef.close();
    }
}
