package pl.softwareskill.course.db.ormframeworks.hibernate.pure;

import java.util.Collection;
import static java.util.Objects.nonNull;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardRepository;
import pl.softwareskill.course.db.ormframeworks.hibernate.DatabaseOperationException;

@RequiredArgsConstructor
class PureHibernateNoTransactionsCardRepository implements CardRepository {

    private final SessionFactory factory;

    @Override
    public Optional<Card> findById(String cardId) {
        Transaction tx = null;
        try {
            var session = factory.getCurrentSession();

            //bez transakcji natywne Hibernate rzuci wyjątek
            tx = session.beginTransaction();

            Card value = session.get(Card.class, cardId);

            //commit(tx);

            return Optional.ofNullable(value);
        } catch (Exception e) {
            rollback(tx);
            throw new DatabaseOperationException(e);
        }
    }

    private void commit(Transaction tx) {
        if (nonNull(tx) && tx.isActive()) {
            tx.commit();
        }
    }

    private void rollback(Transaction tx) {
        if (nonNull(tx) && tx.isActive()) {
            tx.rollback();
        }
    }

    @Override
    public void update(Card card) {
        Transaction tx = null;
        try {
            var session = factory.getCurrentSession();

            tx = session.beginTransaction();

            session.update(card);

            commit(tx);
        } catch (HibernateException e) {
            rollback(tx);
            throw new DatabaseOperationException(e);
        }
    }

    @Override
    public void persist(Card card) {
        Transaction tx = null;
        try {
            var session = factory.getCurrentSession();

            tx = session.beginTransaction();

            session.persist(card);

            commit(tx);
        } catch (HibernateException e) {
            rollback(tx);
            throw new DatabaseOperationException(e);
        }
    }

    @Override
    public Collection<Card> findAll() {
        Transaction tx = null;
        try (var session = factory.getCurrentSession()) {
            tx = session.beginTransaction();

            var query = session.createQuery("FROM Cards", Card.class);

            var list = query.getResultList();

            commit(tx);

            return list;
        } catch (Exception e) {
            rollback(tx);
            throw new DatabaseOperationException(e);
        }
    }
}