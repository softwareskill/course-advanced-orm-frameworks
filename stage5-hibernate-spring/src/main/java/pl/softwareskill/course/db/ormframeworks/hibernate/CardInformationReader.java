package pl.softwareskill.course.db.ormframeworks.hibernate;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
class CardInformationReader {

    CardInformationLogger logger;
    CardRepository cardRepository;

    void printCardData(String cardId) {
        cardRepository.findById(cardId)
                .ifPresentOrElse(logger::logCardData,
                        () -> logger.logCardDataNotFound(cardId));
    }
}
