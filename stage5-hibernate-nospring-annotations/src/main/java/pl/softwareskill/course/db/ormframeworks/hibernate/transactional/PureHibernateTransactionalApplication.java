package pl.softwareskill.course.db.ormframeworks.hibernate.transactional;

import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardCountry;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationLogger;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationReader;
import pl.softwareskill.course.db.ormframeworks.hibernate.HibernateInitializer;
import pl.softwareskill.course.db.ormframeworks.hibernate.UserInformationReader;

public class PureHibernateTransactionalApplication {

    public static void main(String[] args) {
        //initialize Hibernate
        var sessionFactory = HibernateInitializer.initialize();
        Session session = sessionFactory.openSession();

        CardInformationLogger logger = new CardInformationLogger();
        PureHibernateTransactionalCardRepository cardRepository = new PureHibernateTransactionalCardRepository(session);
        PureHibernateTransactionalUserRepository userRepository = new PureHibernateTransactionalUserRepository(session);

        if ("p".equals(args[0])) {
            new CardInformationReader(logger, cardRepository).printCardData(args[1]);
        } else if ("e".equals(args[0])) {
            new PureHibernateTransactionalCardInformationEditor(logger, cardRepository, session).changeCardOwner(args[1], args[2]);
        } else if ("a".equals(args[0])) {
            new PureHibernateTransactionalCardCreator(logger, cardRepository, sessionFactory).addNewCard(args[1], args[2], args[3],
                    Boolean.parseBoolean(args[4]), CardCountry.valueOf(args[5]));
        } else if ("d".equals(args[0])) {
            new PureHibernateTransactionalCardDuplicator(cardRepository, session).duplicateCard(args[1], args[2]);
        } else if ("db".equals(args[0])) {
            new PureHibernateTransactionalCardsBatchDisable(cardRepository, session).disableActiveUserCards(args[1]);
        } else if ("u".equals(args[0])) {
            new UserInformationReader(userRepository).printUserData(args[1]);
        }

        //close resources
        session.close();
        sessionFactory.close();
    }
}
