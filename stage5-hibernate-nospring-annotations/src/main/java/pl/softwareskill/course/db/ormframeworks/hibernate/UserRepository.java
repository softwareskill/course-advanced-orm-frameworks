package pl.softwareskill.course.db.ormframeworks.hibernate;

import java.util.Optional;

public interface UserRepository {

    Optional<User> findById(String userId);
}
