package pl.softwareskill.course.db.ormframeworks.hibernate.jpa;

import java.util.Optional;
import javax.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardRepository;

@RequiredArgsConstructor
class JpaTransactionalCardRepository implements CardRepository {

    private final EntityManager entityManager;

    @Override
    public Optional<Card> findById(String cardId) {

        Card value = entityManager.find(Card.class, cardId);
        return Optional.ofNullable(value);
    }

    @Override
    public void update(Card card) {
        entityManager.merge(card);
    }

    @Override
    public void persist(Card card) {
        entityManager.persist(card);
    }

    @Override
    public void delete(Card card) {
        entityManager.remove(card);
    }
}