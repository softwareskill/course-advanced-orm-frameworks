package pl.softwareskill.course.db.ormframeworks.hibernate;

import java.util.Optional;
import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Repository;

@Repository
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
class CardRepository {

    EntityManager entityManager;

    Optional<Card> findById(String cardId) {
        return Optional.ofNullable(entityManager.find(Card.class, cardId));
    }
}