package pl.softwareskill.course.db.ormframeworks.hibernate.jpa;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationLogger;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardRepository;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
class JpaTransactionalCardInformationEditor {

    CardInformationLogger logger;
    CardRepository cardRepository;
    EntityManager entityManager;

    void changeCardOwner(String cardId, String ownerName) {
        var transaction = entityManager.getTransaction();
        transaction.begin();
        AtomicBoolean commit = new AtomicBoolean(false);
        try {
            cardRepository.findById(cardId)
                    .ifPresentOrElse(card -> {
                                card.setCardOwner(ownerName);
                                //don't need to update - object is persistent, will be updated after commit
                                //cardRepository.update(card);
                                logger.logCardDataChanged(card);
                                commit.set(true);
                            },
                            () -> logger.logCardDataNotFound(cardId));
        } catch (Exception e) {
            log.error("Błąd edycji danych", e);
        }
        if (commit.get()) {
            transaction.commit();
        } else {
            transaction.rollback();
        }
    }
}
