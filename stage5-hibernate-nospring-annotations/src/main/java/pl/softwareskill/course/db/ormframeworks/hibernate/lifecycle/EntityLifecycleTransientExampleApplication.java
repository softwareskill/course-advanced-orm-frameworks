package pl.softwareskill.course.db.ormframeworks.hibernate.lifecycle;

import java.util.UUID;
import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardCountry;
import pl.softwareskill.course.db.ormframeworks.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje natywne Hibernate dla stanu encji transient.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext
 * oraz co się stanie po wywołaniu różnych metod dla encji transient (w zależności
 * czy istnieje rekord skojarzony z id encji lub nie)
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class EntityLifecycleTransientExampleApplication {

    public static void main(String[] args) {
        //initialize Hibernate
        var sessionFactory = HibernateInitializer.initializeForPostgreSql();
        Session session = sessionFactory.openSession();

        var tx = session.beginTransaction();

//        var cardEntity = session.find(Card.class, "1");
        var cardEntity = new Card();
        cardEntity.setCardId("1" + System.currentTimeMillis());
        cardEntity.setCardUuid(UUID.randomUUID().toString());
        cardEntity.setEnabled(false);
        cardEntity.setCardCountry(CardCountry.DE);
        cardEntity.setCardOwner("1");
//
        session.persist(cardEntity);
        //cardEntity.setCardCountry(CardCountry.DE);
        //session.update(cardEntity);//update usuniętej encji rzuci wyjątek
        //session.evict(cardEntity);

//        session.remove(cardEntity);//remove - evict - remove  wywala się

        //Jeżeli zmodyfikujesz dane encji to podczas wyszukiwania encja nie zostanie pobrana z bazy a z cache (PC)
        var list = session.createQuery("FROM Card").getResultList();

        //Użyj flush jeśli chcesz zobaczyć jakie zmiany zostaną wysłane do bazy danych
        //session.flush();

        tx.rollback();//Celowe aby można było wielokrotnie wywoływać ten sam kod

        //close resources
        session.close();
        sessionFactory.close();
    }
}
