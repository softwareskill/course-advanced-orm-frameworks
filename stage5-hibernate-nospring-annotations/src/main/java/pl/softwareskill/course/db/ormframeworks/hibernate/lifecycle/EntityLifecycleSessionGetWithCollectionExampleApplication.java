package pl.softwareskill.course.db.ormframeworks.hibernate.lifecycle;

import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.HibernateInitializer;
import pl.softwareskill.course.db.ormframeworks.hibernate.User;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje natywne Hibernate dla stanu encji z relacją do wielu
 * i zaczytywanie kolekcji persystentnych.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext
 * oraz co się stanie po wywołaniu metod związanych z usuwaniem encji (W tym
 * takiej, która nie została jeszcze pobrana z bazy).
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class EntityLifecycleSessionGetWithCollectionExampleApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate - zobacz pliki hibernate.postgres.cfg.xml oraz hibernate.properties
        var sessionFactory = HibernateInitializer.initialize();

        Session session = sessionFactory.openSession();

        //Zobacz kolekcje persystentne w PersistenceContext
        var userEntity = session.get(User.class, "1");

        //close resources
        session.close();
        sessionFactory.close();
    }
}
