package pl.softwareskill.course.db.ormframeworks.hibernate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CardInformationLogger {

    public void logCardData(Card card) {
        log.info("Card data found for id={} is uuid={}, balance={}, active={}, country={}", card.getCardId(), card.getCardUuid(),
                card.getCardBalance(), card.getEnabled(), card.getCardCountry());
    }

    public void logCardDataNotFound(String cardId) {
        log.error("Card data for id={} doesn't exist", cardId);
    }

    public void logCardDataChanged(Card card) {
        log.info("Card data changed uuid={}, balance={}, active={}, country={}", card.getCardUuid(),
                card.getCardBalance(), card.getEnabled(), card.getCardCountry());
    }

    public void logCardDataCreated(Card card) {
        log.info("New card added id={} is uuid={}, balance={}, active={}, country={}", card.getCardId(), card.getCardUuid(),
                card.getCardBalance(), card.getEnabled(), card.getCardCountry());
    }

    public void logCardDeleted(String cardId) {
        log.info("Card successfully removed id={}", cardId);
    }
}
