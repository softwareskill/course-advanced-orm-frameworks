package pl.softwareskill.course.db.ormframeworks.hibernate;

import java.util.Optional;
import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Repository;

@Repository
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
class UserRepository {

    EntityManager entityManager;

    Optional<User> findById(Long userId) {
        return Optional.ofNullable(entityManager.find(User.class, userId));
    }

    void save(User user) {
        entityManager.merge(user);
    }
}