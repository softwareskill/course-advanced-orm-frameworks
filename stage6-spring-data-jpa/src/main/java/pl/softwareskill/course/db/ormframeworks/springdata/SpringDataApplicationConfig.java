package pl.softwareskill.course.db.ormframeworks.springdata;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories
class SpringDataApplicationConfig {

    @Bean
    CardInformationLogger cardInformationLogger() {
        return new CardInformationLogger();
    }

    @Bean
    CardInformationReader cardInformationReader(CardInformationLogger cardInformationLogger, CardRepository cardRepository) {
        return new CardInformationReader(cardInformationLogger, cardRepository);
    }
}
