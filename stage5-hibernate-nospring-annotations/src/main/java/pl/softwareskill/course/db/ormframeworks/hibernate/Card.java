package pl.softwareskill.course.db.ormframeworks.hibernate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "CARDS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Card {
    @Id
    @Column(name = "CARD_ID")
    private String cardId;

    @Column(name = "CARD_UUID")
    private String cardUuid;

    @Column(name = "CARD_OWNER_ID")
    private String cardOwner;

    @Column(name = "ENABLED")
    @Convert(converter = YesNoBooleanConverter.class)
    private Boolean enabled;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    private CardCountry cardCountry;
}
