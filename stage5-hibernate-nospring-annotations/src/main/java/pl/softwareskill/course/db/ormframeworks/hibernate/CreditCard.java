package pl.softwareskill.course.db.ormframeworks.hibernate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "CARDS")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "owner")
public class CreditCard {
    @Id
    @Column(name = "CARD_ID")
    String cardId;

    @Column(name = "CARD_UUID")
    String cardUuid;

    @Column(name = "ENABLED")
    @Convert(converter = YesNoBooleanConverter.class)
    boolean enabled;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    CardCountry cardCountry;

    @ManyToOne
    @JoinColumn(name = "CARD_OWNER_ID")
    User owner;
}
