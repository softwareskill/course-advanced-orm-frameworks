package pl.softwareskill.course.db.ormframeworks.springdata;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
class Address {

    @Column(name = "STREET")
    String street;

    @Column(name = "STREET_NUMBER")
    String number;

    @Column(name = "CITY")
    String city;

    @Column(name = "COUNTRY")
    String country;

    @Column(name = "POSTAL_CODE")
    String postalCode;
}
