package pl.softwareskill.course.db.ormframeworks.springdata;

class CardInformationReader {

    private final CardInformationLogger logger;
    private final CardRepository cardRepository;

    CardInformationReader(CardInformationLogger logger, CardRepository cardRepository) {
        this.logger = logger;
        this.cardRepository = cardRepository;
    }

    void printCardData(String cardId) {
        cardRepository.findById(cardId)
                .ifPresentOrElse(logger::logCardData,
                        () -> logger.logCardDataNotFound(cardId));
    }
}
