package pl.softwareskill.course.db.ormframeworks.springdata;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface CardRepository extends JpaRepository<Card, String> {

    List<Card> findAllByCardIdAndEnabledAndCardCountry(String cardId, Boolean enabled, CardCountry cardCountry);
}