package pl.softwareskill.course.db.ormframeworks.hibernate.jpa;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardCountry;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationLogger;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationReader;

public class JpaPersistenceHibernateApplication {

    public static void main(String[] args) {
        //create JPA EntityManagerFactory
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("SoftwareSkill");
        //create EntityManager
        var entityManager = entityManagerFactory.createEntityManager();

        CardInformationLogger logger = new CardInformationLogger();
        JpaTransactionalCardRepository cardRepository = new JpaTransactionalCardRepository(entityManager);
        if ("p".equals(args[0])) {
            new CardInformationReader(logger, cardRepository).printCardData(args[1]);
        } else if ("e".equals(args[0])) {
            new JpaTransactionalCardInformationEditor(logger, cardRepository, entityManager).changeCardOwner(args[1], args[2]);
        } else if ("a".equals(args[0])) {
            new JpaTransactionalCardCreator(logger, cardRepository, entityManager).addNewCard(args[1], args[2], args[3],
                    Boolean.parseBoolean(args[4]), CardCountry.valueOf(args[5]));
        } else if ("d".equals(args[0])) {
            new JpaTransactionalCardRemover(logger, cardRepository, entityManager).removeCard(args[1]);
        }
        //close resources
        entityManager.close();
    }
}
