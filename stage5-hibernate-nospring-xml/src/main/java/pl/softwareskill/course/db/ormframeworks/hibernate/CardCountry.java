package pl.softwareskill.course.db.ormframeworks.hibernate;

public enum CardCountry {
    PL,
    EN,
    DE
}
