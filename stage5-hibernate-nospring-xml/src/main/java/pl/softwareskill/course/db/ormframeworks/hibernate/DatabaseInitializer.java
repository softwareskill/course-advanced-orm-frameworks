package pl.softwareskill.course.db.ormframeworks.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DatabaseInitializer {

    public static SessionFactory initializeFromHbmXml() {
        try {
            return new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            throw new pl.softwareskill.course.db.ormframeworks.hibernate.DatabaseOperationException(ex);
        }
    }

    public static SessionFactory initializeFromProperties() {
        try {
            var configuration = new Configuration();
            configuration.addResource("Card.hbm.xml");
            return configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new pl.softwareskill.course.db.ormframeworks.hibernate.DatabaseOperationException(ex);
        }
    }
}
