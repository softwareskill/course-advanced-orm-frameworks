package pl.softwareskill.course.db.ormframeworks.hibernate.lifecycle;

import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje natywne Hibernate dla stanu encji detached.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext
 * oraz co się stanie po wywołaniu detach/evict.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class EntityLifecycleDetachedExampleApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate - zobacz pliki hibernate.cfg.xml oraz hibernate.properties
        var sessionFactory = HibernateInitializer.initializeForPostgreSql();

        Session session = sessionFactory.openSession();//to samo można z enitymanager

        //rozpoczęcie transakcji
        var tx = session.getTransaction();
        tx.begin();

        var cardEntity = session.find(Card.class, "1");

        //Zmień kolejnośc detached z innymi operacjami i zobacz co się stanie
        session.detach(cardEntity);//detach

        session.delete(cardEntity);//remove

        //Wymuszenie wysłania zmian do bazy danych (wydrukowanie zapytań SQL)
        session.flush();

        tx.rollback();//Celowe aby można było wielokrotnie wywoływać ten sam kod

        //close resources
        session.close();
        sessionFactory.close();
    }
}
