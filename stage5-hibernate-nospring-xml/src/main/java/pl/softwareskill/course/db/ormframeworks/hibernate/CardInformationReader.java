package pl.softwareskill.course.db.ormframeworks.hibernate;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
class CardInformationReader {

    CardInformationLogger logger;
    CardRepository cardRepository;

    void printCardData(String cardId) {
        cardRepository.findById(cardId)
                .ifPresentOrElse(logger::logCardData,
                        () -> logger.logCardDataNotFound(cardId));
    }
}
