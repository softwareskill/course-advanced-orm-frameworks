package pl.softwareskill.course.db.ormframeworks.hibernate;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje Hibernate w trybie JPA z poziomu Spring
 * z wykorzystaniem CrudRepository ze SpringData
 *
 * Uruchomienie wymaga podania jednego parametru - inentyfikatora karty do wyszukania
 */
@SpringBootApplication
@EnableTransactionManagement
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class HibernateApplication implements CommandLineRunner {

    CardInformationReader informationReader;
    EntityDetachCaller detachCaller;
    EntityManagerClearCaller clearCaller;
    EntityManagerCloseCaller closeCaller;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplicationBuilder()
                .sources(HibernateApplication.class)
                .web(WebApplicationType.NONE)
                .build();
        app.run(args).close();
    }

    @Override
    public void run(String... args) {
        if ("print".equals(args[0])) {
            informationReader.printCardData(args[1]);
        } else if ("detach".equals(args[0])) {
            detachCaller.detachTest(args[1]);
        } else if ("clear".equals(args[0])) {
            clearCaller.clearTest(args[1]);
        } else if ("close".equals(args[0])) {
            closeCaller.closeTest(args[1]);
        } else {
            log.error("Nieprawidłowe parametry");
        }
    }
}
