package pl.softwareskill.course.db.ormframeworks.hibernate.jpa;

import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationLogger;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardRepository;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
class JpaTransactionalCardRemover {

    CardInformationLogger logger;
    CardRepository cardRepository;
    EntityManager entityManager;

    void removeCard(String cardId) {
        var transaction = entityManager.getTransaction();
        transaction.begin();
        //Only for test
        //Puts existing entity in cache and allows primary key duplication validation by Hibernate
        //instead of waiting for primary key constraint validation by database
        try {
            cardRepository.findById(cardId)
                    .ifPresentOrElse(card -> {
                        Card c = new Card();
                        c.setCardId(cardId);
                        cardRepository.delete(card);
                        transaction.commit();
                        logger.logCardDeleted(cardId);
                    }, () -> logger.logCardDataNotFound(cardId));
        } catch (Exception e) {
            log.error("Błąd usuwania karty", e);
            transaction.rollback();
        }
    }
}
