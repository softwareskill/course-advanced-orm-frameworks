package pl.softwareskill.course.db.ormframeworks.hibernate.jpa;

import javax.persistence.EntityManager;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardCountry;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationLogger;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardRepository;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
class JpaTransactionalCardCreator {

    CardInformationLogger logger;
    CardRepository cardRepository;
    EntityManager entityManager;

    void addNewCard(String cardId, String ownerName, String uuid, boolean enabled, CardCountry cardCountry) {
        var transaction = entityManager.getTransaction();
        transaction.begin();
        //Only for test
        //Puts existing entity in cache and allows primary key duplication validation by Hibernate
        //instead of waiting for primary key constraint validation by database
        cardRepository.findById(cardId);

        Card card = new Card();
        card.setCardId(cardId);
        card.setEnabled(enabled);
        card.setCardCountry(cardCountry);
        card.setCardUuid(uuid);
        card.setCardOwner(ownerName);

        try {
            cardRepository.update(card);
            transaction.commit();
            logger.logCardDataCreated(card);
        } catch (Exception e) {
            log.error("Błąd dodawania karty", e);
            transaction.rollback();
        }
    }
}
