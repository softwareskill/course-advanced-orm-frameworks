package pl.softwareskill.course.db.ormframeworks.hibernate;

public class DatabaseOperationException extends RuntimeException {
    public DatabaseOperationException(Throwable cause) {
        super(cause);
    }
}
