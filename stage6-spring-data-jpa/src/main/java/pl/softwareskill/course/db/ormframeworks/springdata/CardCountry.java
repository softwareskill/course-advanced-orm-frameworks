package pl.softwareskill.course.db.ormframeworks.springdata;

public enum CardCountry {
    PL,
    EN,
    DE
}
