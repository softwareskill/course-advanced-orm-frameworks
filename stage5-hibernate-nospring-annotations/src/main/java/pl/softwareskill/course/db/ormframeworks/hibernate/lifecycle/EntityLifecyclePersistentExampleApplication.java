package pl.softwareskill.course.db.ormframeworks.hibernate.lifecycle;

import java.util.List;
import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje natywne Hibernate dla stanu encji persistent/managed.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext
 * oraz co się stanie po wywołaniu metod get (w tym wielokrotnych), jakie zapytania się wygenerują.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class EntityLifecyclePersistentExampleApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate - zobacz pliki hibernate.postgres.cfg.xml oraz hibernate.properties
        var sessionFactory = HibernateInitializer.initializeForPostgreSql();
        Session session = sessionFactory.openSession();

        var cardEntity = session.get(Card.class, "1");

        var cardEntity2 = session.get(Card.class, "1");//za drugim razem encja zostanie pobrana z cache

        System.out.println("Encje są równe " + (cardEntity == cardEntity2));

        //Pobieramy karty ale w liście zostanie zwrócona wcześniej pobrana karta
        List<Card> cards = session.createQuery("FROM Card", Card.class).getResultList();

        //close resources
        session.close();
        sessionFactory.close();
    }
}
