package pl.softwareskill.course.db.ormframeworks.hibernate.lifecycle;

import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje natywne Hibernate dla stanu encji removed.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext
 * oraz co się stanie po wywołaniu metod związanych z usuwaniem encji (W tym
 * takiej, która nie została jeszcze pobrana z bazy).
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class EntityLifecycleRemovedExampleApplication {

    public static void main(String[] args) {

        //Inicjalizacja Hibernate - zobacz pliki hibernate.cfg.xml oraz hibernate.properties
        var sessionFactory = HibernateInitializer.initialize();
        Session session = sessionFactory.openSession();

        var tx = session.beginTransaction();

        var cardEntity = session.get(Card.class, "1");
        //Zobacz co się stanbie jak usuniesz encję typu transient, o istniejącym identyfikatorze
//        var cardEntity = new Card();
//        cardEntity.setCardId("1");
//        cardEntity.setCardId("1asdasddd");

        session.evict(cardEntity);

        session.delete(cardEntity); //delete po evict przywraca encję do działania

        //session.update(cardEntity); //zobacz czy update nadpisze delete

        //Wysłanie zmian do bazy danych
        session.flush();

        tx.rollback();//Celowe aby można było wielokrotnie wywoływać ten sam kod

        //close resources
        session.close();
        sessionFactory.close();
    }
}
