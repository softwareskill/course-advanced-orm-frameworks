package pl.softwareskill.course.db.ormframeworks.springdata;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByLastNameAndAddress_StreetAndAddress_Number(String lastName, String street, String number);
}