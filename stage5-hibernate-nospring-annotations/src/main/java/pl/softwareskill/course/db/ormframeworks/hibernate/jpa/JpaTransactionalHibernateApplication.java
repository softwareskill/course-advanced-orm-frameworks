package pl.softwareskill.course.db.ormframeworks.hibernate.jpa;

import pl.softwareskill.course.db.ormframeworks.hibernate.CardCountry;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationLogger;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationReader;
import pl.softwareskill.course.db.ormframeworks.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje Hibernate w trybie natywnym z wykorzystaniem EntityManager (nie jest to pełny tryb JPA)
 * - Session implementuje EntityManager
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext.
 *
 * Uruchomienie wymaga parameytru operacji oraz odpowiednich dalszych parametrów
 * p - wyszukanie danych karty - wymaga podania id karty do wyszukania
 * e - edycja danych karty (zmiana właściciela) - wymaga podania dwóch parametrów
 *   - id karty do wyszukania
 *   - id użytkownika do zmiany
 * a - utworzenie nowej karty wymaga podania dodatkowo 4ch parametrów
 *   id karty
 *   id właściciela
 *   uuid karty
 *   flaga aktywności (true/false)
 *   kraj karty
 */
public class JpaTransactionalHibernateApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate - zobacz pliki hibernate.cfg.xml oraz hibernate.properties
        var sessionFactory = HibernateInitializer.initialize();

        //popranie EntityManager - uwaga to nie tryb JPA
        var entityManager = sessionFactory.createEntityManager();

        CardInformationLogger logger = new CardInformationLogger();
        JpaTransactionalCardRepository cardRepository = new JpaTransactionalCardRepository(entityManager);
        if ("p".equals(args[0])) {
            new CardInformationReader(logger, cardRepository).printCardData(args[1]);
        } else if ("e".equals(args[0])) {
            new JpaTransactionalCardInformationEditor(logger, cardRepository, entityManager).changeCardOwner(args[1], args[2]);
        } else if ("a".equals(args[0])) {
            new JpaTransactionalCardCreator(logger, cardRepository, entityManager).addNewCard(args[1], args[2], args[3],
                    Boolean.parseBoolean(args[4]), CardCountry.valueOf(args[5]));
        }
        //close resources
        entityManager.close();
        sessionFactory.close();
    }
}
