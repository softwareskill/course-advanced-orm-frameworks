package pl.softwareskill.course.db.ormframeworks.hibernate.transactional;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardRepository;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
class PureHibernateTransactionalCardDuplicator {

    CardRepository cardRepository;
    Session session;

    void duplicateCard(String cardId, String newCardId) {
        var transaction = session.getTransaction();
        transaction.begin();
        try {
            cardRepository.duplicateCard(cardId, newCardId);
            transaction.commit();
        } catch (Exception e) {
            log.error("Błąd klonowania karty", e);
            transaction.rollback();
        }
    }
}
