package pl.softwareskill.course.db.ormframeworks.hibernate;

class DatabaseOperationException extends RuntimeException {
    public DatabaseOperationException(Throwable cause) {
        super(cause);
    }
}
