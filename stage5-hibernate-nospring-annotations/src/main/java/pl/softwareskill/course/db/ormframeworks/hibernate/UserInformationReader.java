package pl.softwareskill.course.db.ormframeworks.hibernate;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
public class UserInformationReader {

    UserRepository userRepository;

    public void printUserData(String userId) {
        userRepository.findById(userId)
                .ifPresent(user -> System.out.println("Dane użytkownika " + user));
    }
}
