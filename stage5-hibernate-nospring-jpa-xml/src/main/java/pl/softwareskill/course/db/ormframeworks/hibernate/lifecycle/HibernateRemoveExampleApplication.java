package pl.softwareskill.course.db.ormframeworks.hibernate.lifecycle;

import javax.persistence.Persistence;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje Hibernate w trybie JPA dla stanu encji removed.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext
 * oraz co się stanie po wywołaniu remove.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class HibernateRemoveExampleApplication {

    public static void main(String[] args) {

        //Konfiguracja - zobacz META-INF/persistence.xml
        var ef = Persistence.createEntityManagerFactory("SoftwareSkill");

        var em = ef.createEntityManager();

        var tx = em.getTransaction();
        tx.begin();

        var card = em.find(Card.class, "1");
        //var card = new Card();
        //card.setCardId("1");//istniejąca ale transient
        //card.setCardId("12222222");//nie istniejąca transient
        em.detach(card);//Wypinając usunięty wcześniej anulujesz usunięcie

        em.remove(card);

        em.flush();//sprawdź czy flush zadziała bez transakcji

        em.close();

        ef.close();
    }
}
