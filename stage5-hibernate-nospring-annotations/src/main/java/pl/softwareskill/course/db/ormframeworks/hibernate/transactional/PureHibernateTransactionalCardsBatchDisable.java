package pl.softwareskill.course.db.ormframeworks.hibernate.transactional;

import static java.util.Objects.nonNull;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardRepository;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
class PureHibernateTransactionalCardsBatchDisable {

    CardRepository cardRepository;
    Session session;

    void disableActiveUserCards(String ownerName) {
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            ((PureHibernateTransactionalCardRepository) cardRepository).disableUserCardsInBatches(ownerName);
            transaction.commit();
        } catch (Exception e) {
            log.error("Błąd deaktywacji kart", e);
            if (nonNull(transaction)) {
                transaction.rollback();
            }
        }
    }
}
