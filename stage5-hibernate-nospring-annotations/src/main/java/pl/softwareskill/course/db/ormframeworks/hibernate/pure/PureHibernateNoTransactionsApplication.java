package pl.softwareskill.course.db.ormframeworks.hibernate.pure;

import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationLogger;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationReader;
import pl.softwareskill.course.db.ormframeworks.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje natywne Hibernate oraz odczyt encji poprzez własne Dao/Repository.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext.
 *
 * Uruchomienie wymaga podania jednego parametru - identyfikatora karty do wyszukania
 */
public class PureHibernateNoTransactionsApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate - zobacz pliki hibernate.cfg.xml oraz hibernate.properties
        var sessionFactory = HibernateInitializer.initialize();

        CardInformationLogger logger = new CardInformationLogger();

        //Repozytorium danych kart
        PureHibernateNoTransactionsCardRepository cardRepository = new PureHibernateNoTransactionsCardRepository(sessionFactory);

        CardInformationReader cardInformationReader = new CardInformationReader(logger, cardRepository);

        cardInformationReader.printCardData(args[0]);

        //close resources
        sessionFactory.close();
    }

}
