package pl.softwareskill.course.db.ormframeworks.hibernate;

import java.util.Collection;
import java.util.Optional;

public interface CardRepository {
    Optional<Card> findById(String cardId);

    Collection<Card> findAll();

    void update(Card card);

    void persist(Card card);

    default void duplicateCard(String sourceId, String destinationId) {
        findById(sourceId)
                .ifPresentOrElse(card -> {
                    Card clone = new Card();
                    clone.setCardOwner(card.getCardOwner());
                    clone.setCardUuid(card.getCardUuid());
                    clone.setCardCountry(card.getCardCountry());
                    clone.setEnabled(card.getEnabled());
                    clone.setCardId(destinationId);
                    persist(card);
                }, () -> {
                    throw new DatabaseOperationException(new IllegalStateException("Nie znaleziono karty"));
                });
    }
}
