package pl.softwareskill.course.db.ormframeworks.hibernate.lifecycle;

import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.Persistence;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardCountry;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Prezentuje Hibernate w trybie JPA dla stanu encji transient.
 *
 * W trybie debug można zobaczyć jakie klasy znają się w PersistenceContext
 * oraz co się stanie po wywołaniu róznych metod EntityManager na encji transient
 * w zależności od tego czy reprtezentuje ona istniejący czy nowy rekord w bazie.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class HibernateTransientExampleApplication {

    public static void main(String[] args) {

        //Konfiguracja - zobacz META-INF/persistence.xml
        var ef = Persistence.createEntityManagerFactory("SoftwareSkill");

        var em = ef.createEntityManager();

        var tr = em.getTransaction();
        tr.begin();

        var card = new Card();
        card.setCardId(String.valueOf(System.currentTimeMillis()));
//        card.setCardId("1");
        card.setCardCountry(CardCountry.PL);
        card.setCardUuid(UUID.randomUUID().toString());
        card.setEnabled(false);
        card.setCardBalance(BigDecimal.TEN);

        em.detach(card);

        em.persist(card);

        em.flush();

        em.close();

        ef.close();
    }
}
