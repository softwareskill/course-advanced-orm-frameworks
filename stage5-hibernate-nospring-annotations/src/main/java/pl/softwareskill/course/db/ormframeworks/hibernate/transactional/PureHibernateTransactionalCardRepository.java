package pl.softwareskill.course.db.ormframeworks.hibernate.transactional;

import java.util.Collection;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.Card;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardRepository;
import pl.softwareskill.course.db.ormframeworks.hibernate.DatabaseOperationException;

@RequiredArgsConstructor
class PureHibernateTransactionalCardRepository implements CardRepository {

    private final Session session;

    @Override
    public Optional<Card> findById(String cardId) {
        try {
            Card value = session.get(Card.class, cardId);
            return Optional.ofNullable(value);
        } catch (Exception e) {
            throw new DatabaseOperationException(e);
        }
    }

    public void disableCard(String cardId) {
        try {
            var card = session.get(Card.class, cardId);
            if (card == null) {
                throw new DatabaseOperationException(new IllegalStateException("Nie znaleziono karty"));
            }
            card.setEnabled(false);
            //session.update(card); to jest zbędne PersistenceContext wykryje zmiany
        } catch (Exception e) {
            throw new DatabaseOperationException(e);
        }
    }

    @Override
    public void update(Card card) {
        try {
            session.update(card);
        } catch (HibernateException e) {
            throw new DatabaseOperationException(e);
        }
    }

    public void disableUserCards(String ownerId) {
        try {
            var query = session.createQuery(
                    "UPDATE Card set enabled=false where cardOwner=:ownerId");
            query.setParameter("ownerId", ownerId);
            query.executeUpdate();
        } catch (HibernateException e) {
            throw new DatabaseOperationException(e);
        }
    }

    public void disableUserCardsInBatches(String ownerName) {
        try {
            final int batchSize = 10;
            //Mamy 50 rekordów
            for (int i = 0; i < 5; i++) {
                var query = session.createQuery("FROM Card where enabled=true", Card.class);
                //Pobieramy kwałkami
                query.setMaxResults(batchSize);
                //Przesuwamy się na kolejny kawałek
                query.setFirstResult(i * batchSize);
                //Iteracja po liście i zmiana danych dla określonego użytkownika
                var result = query.getResultList();
                //dla i == 3 wynik nie zostanie zaleziony
                result.stream()
                        .filter(card -> ownerName.equals(card.getCardOwner()))
                        .forEach(card -> card.setEnabled(false));
            }
        } catch (HibernateException e) {
            throw new DatabaseOperationException(e);
        }
    }

    @Override
    public void persist(Card card) {
        try {
            session.persist(card);
        } catch (HibernateException e) {
            throw new DatabaseOperationException(e);
        }
    }

    public void delete(Card card) {
        try {
            session.delete(card);
        } catch (HibernateException e) {
            throw new DatabaseOperationException(e);
        }
    }

    public void deleteUserCards(String ownerId) {
        try {
            var query = session.createQuery("DELETE FROM Card where cardOwner=:ownerId");
            query.setParameter(1, ownerId);
            query.executeUpdate();
        } catch (HibernateException e) {
            throw new DatabaseOperationException(e);
        }
    }

    public void deleteUserCardsByCriteria(String ownerId) {
        //Tworzymy Builder
        var criteriaBuilder = session.getCriteriaBuilder();
        //CriteriaDelete dla klasy Card
        var criteriaDelete = criteriaBuilder.createCriteriaDelete(Card.class);
        //Fraza FROM
        var delete = criteriaDelete.from(Card.class);
        //Fraza Where
        criteriaDelete.where(criteriaBuilder.equal(delete.get("cardOwner"), ownerId));
        //Wykonanie operacji
        session.createQuery(criteriaDelete).executeUpdate();
    }

    @Override
    public Collection<Card> findAll() {
        try {
            var query = session.createQuery("FROM Card", Card.class);
            return query.getResultList();
        } catch (Exception e) {
            throw new DatabaseOperationException(e);
        }
    }

    @Override
    public void duplicateCard(String sourceId, String destinationId) {
        try {
            var query = session.createQuery(
                    "insert into Card(cardId,cardUuid,cardOwner,enabled,cardCountry) " +
                            "select :destinationId,cardUuid,cardOwner,enabled,cardCountry from Card " +
                            "where cardId=:sourceId");
            query.setParameter("destinationId", destinationId);
            query.setParameter("sourceId", sourceId);
            var insertedCount = query.executeUpdate();
            if (insertedCount != 1) {
                throw new IllegalStateException("Operacja kopiowania karty nieudana");
            }
        } catch (Exception e) {
            throw new DatabaseOperationException(e);
        }
    }
}