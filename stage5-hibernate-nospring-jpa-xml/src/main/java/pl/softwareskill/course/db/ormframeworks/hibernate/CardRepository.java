package pl.softwareskill.course.db.ormframeworks.hibernate;

import java.util.Optional;

public interface CardRepository {
    Optional<Card> findById(String cardId);

    void update(Card card);

    void persist(Card card);

    void delete(Card card);
}
