package pl.softwareskill.course.db.ormframeworks.hibernate.transactional;

import java.util.concurrent.atomic.AtomicBoolean;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardInformationLogger;
import pl.softwareskill.course.db.ormframeworks.hibernate.CardRepository;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
class PureHibernateTransactionalCardInformationEditor {

    CardInformationLogger logger;
    CardRepository cardRepository;
    Session session;

    void changeCardOwner(String cardId, String ownerName) {
        var transaction = session.getTransaction();

        //Ustawiamy timeout
        transaction.setTimeout(1);

        transaction.begin();

        AtomicBoolean commit = new AtomicBoolean(false);

        try {
            Thread.sleep(3000); //czekamy 3s
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        cardRepository.findById(cardId)
                .ifPresentOrElse(card -> {
                            card.setCardOwner(ownerName);
                            //don't need to update - object is persistent, will be updated after commit
                            //cardRepository.update(card);
                            logger.logCardDataChanged(card);
                            commit.set(true);
                        },
                        () -> logger.logCardDataNotFound(cardId));
        if (commit.get()) {
            transaction.commit();
        } else {
            transaction.rollback();
        }
    }
}
